package com.example.spacextasks.controller;

import com.example.spacextasks.model.AbstractTask;
import com.example.spacextasks.service.TrelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * SpaceX Tasks controller.
 */
@RestController
public class TaskController {

    @Autowired
    private TrelloService trelloService;

    /**
     * Endpoint for task creation.
     *
     * @param task a SpaceX team task.
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<String> createTask(@Valid @RequestBody AbstractTask task) {

        this.trelloService.createCard(task);

        return ResponseEntity.ok("Trello Task created succesfuly.");
    }
}
