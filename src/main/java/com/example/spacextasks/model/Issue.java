package com.example.spacextasks.model;

import com.example.spacextasks.service.CardBuilder;
import com.julienvey.trello.domain.Card;

import javax.validation.constraints.NotEmpty;

public class Issue extends AbstractTask {

    @NotEmpty
    private String title;

    @NotEmpty
    private String description;

    public Issue(){}

    @Override
    public String getList() {
        return TODO_LIST;
    }

    @Override
    public Card build(CardBuilder cardBuilder) {
        return cardBuilder.issue(this);
    }

    @Override
    public String getLabel() {
        return null;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
