package com.example.spacextasks.model;

import com.example.spacextasks.service.CardBuilder;
import com.julienvey.trello.domain.Card;

import javax.validation.constraints.NotEmpty;

public class Bug extends AbstractTask {

    @NotEmpty
    private String description;

    public Bug(){}

    @Override
    public String getList() {
        return SELECTED_LIST;
    }

    @Override
    public Card build(CardBuilder cardBuilder) {
        return cardBuilder.bug(this);
    }

    @Override
    public String getLabel() {
        return "Bug";
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
