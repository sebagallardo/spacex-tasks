package com.example.spacextasks.model;

import com.example.spacextasks.service.CardBuilder;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.julienvey.trello.domain.Card;

/**
 * Defines the Space X team tasks.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Issue.class, name = "issue"),
        @JsonSubTypes.Type(value = Bug.class, name = "bug"),
        @JsonSubTypes.Type(value = Task.class, name = "task")
})
public abstract class AbstractTask {

    public static final String TODO_LIST = "To Do";
    public static final String SELECTED_LIST = "Selected";

    /**
     * Returns the list name where the task should be created.
     *
     * @return a List name
     */
    public abstract String getList();

    /**
     * Trello label name that should be assigned to the task.
     *
     * @return a Trello label name.
     */
    public abstract String getLabel();

    /**
     * Builds a Trello card.
     *
     * @param cardBuilder Trello Card builder.
     * @return a Trello Card.
     */
    public abstract Card build(CardBuilder cardBuilder);
}
