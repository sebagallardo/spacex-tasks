package com.example.spacextasks.model;

public enum TaskCategory {
    Maintenance, Research, Test
}
