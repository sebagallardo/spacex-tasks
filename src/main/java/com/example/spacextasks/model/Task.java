package com.example.spacextasks.model;

import com.example.spacextasks.service.CardBuilder;
import com.julienvey.trello.domain.Card;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class Task extends AbstractTask {

    @NotEmpty
    private String title;

    @NotNull
    private TaskCategory category;

    public Task(){}

    @Override
    public String getList() {
        return SELECTED_LIST;
    }

    @Override
    public Card build(CardBuilder cardBuilder) {
        return cardBuilder.task(this);
    }

    @Override
    public String getLabel() {
        return this.getCategory().name();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TaskCategory getCategory() {
        return category;
    }

    public void setCategory(TaskCategory category) {
        this.category = category;
    }
}
