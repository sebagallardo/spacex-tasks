package com.example.spacextasks.exception;

public class TrelloMissingElementException extends RuntimeException {

    public TrelloMissingElementException(String message) {
        super(message);
    }
}
