package com.example.spacextasks.configuration;

import com.julienvey.trello.Trello;
import com.julienvey.trello.impl.TrelloImpl;
import com.julienvey.trello.impl.http.ApacheHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Http clients bean configuration.
 */
@Configuration
@PropertySource(value = "classpath:secrets.properties")
public class ClientConfiguration {

    @Value("${trello.trelloKey}")
    private String trelloKey;
    @Value("${trello.trelloAccessToken}")
    private String trelloAccessToken;
    @Value("${trello.trelloBoardName}")
    private String trelloBoardName;

    @Bean
    public Trello getTrelloClient() {
        return new TrelloImpl(this.trelloKey, this.trelloAccessToken, new ApacheHttpClient());
    }
}
