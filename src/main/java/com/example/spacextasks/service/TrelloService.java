package com.example.spacextasks.service;

import com.example.spacextasks.exception.TrelloMissingElementException;
import com.example.spacextasks.model.AbstractTask;
import com.julienvey.trello.Trello;
import com.julienvey.trello.domain.Board;
import com.julienvey.trello.domain.Card;
import com.julienvey.trello.domain.TList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Provides Trello API interaction.
 */
@Component
@PropertySource(value = "classpath:secrets.properties")
public class TrelloService {

    @Autowired
    private Trello trelloClient;

    @Value("${trello.trelloBoardName}")
    private String trelloBoardName;

    private static final Logger LOGGER = LogManager.getLogger(TrelloService.class);

    public TrelloService() {}

    /**
     * Obtains the team main Trello board.
     *
     * @return a Trello Board
     */
    public Board getMainBoard() {
        String user = "me";
        return this.trelloClient.getMemberBoards(user).stream()
                .filter(board -> board.getName().equals(trelloBoardName))
                .findFirst().orElseThrow(() -> new TrelloMissingElementException("Missing board: " + trelloBoardName));
    }

    /**
     * Creates a card in the main board for the given task.
     *
     * @param task team task
     */
    public void createCard(AbstractTask task) {

        Board board = this.getMainBoard();
        CardBuilder cardBuilder = new CardBuilder(board);

        TList taskList = board.fetchLists().stream()
                .filter(list -> task.getList().equals(list.getName()))
                .findFirst().orElseThrow(() -> new TrelloMissingElementException("Missing list '" + task.getList() + "' in your board."));

        Card createdCard = this.trelloClient.createCard(taskList.getId(), task.build(cardBuilder));
        LOGGER.info("Succesfuly created Trello card with id: {}", createdCard.getId());

        /* Adding labels after the creation due to Trello API wrapper limitations. */
        if (task.getLabel() != null) {
            createdCard.addLabels(task.getLabel());
        }
    }
}
