package com.example.spacextasks.service;

import com.example.spacextasks.model.Bug;
import com.example.spacextasks.model.Issue;
import com.example.spacextasks.model.Task;
import com.julienvey.trello.domain.Board;
import com.julienvey.trello.domain.Card;

import java.util.Collections;
import java.util.Random;

/**
 * Trello Card builder for creation.
 */
public class CardBuilder {

    private final Board board;

    CardBuilder(Board board) {
        this.board = board;
    }

    public Card issue(Issue issue) {
        Card card = new Card();
        card.setName(issue.getTitle());
        card.setDesc(issue.getDescription());

        return card;
    }

    public Card bug(Bug bug) {
        Card card = new Card();

        card.setName(this.generateBugRandomTitle());
        card.setDesc(bug.getDescription());

        this.board.fetchMembers().stream()
                .findAny()
                .ifPresent(member ->
                        card.setIdMembers(Collections.singletonList(member.getId()))
                );

        return card;
    }

    public Card task(Task task) {
        Card card = new Card();
        card.setName(task.getTitle());

        return card;
    }

    /**
     * Generates a random title for a bug card. Format: 'bug-{word}-{number}'.
     *
     * @return a random bug title.
     */
    private String generateBugRandomTitle() {
        Random random = new Random();
        StringBuilder randomWord = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            char a = (char) (random.nextInt(26) + 'a');
            randomWord.append(a);
        }
        int randomInt = random.nextInt(100);
        return String.format("bug-%s-%s", randomWord.toString(), randomInt);
    }
}
