package com.example.spacextasks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpaceXTasksApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpaceXTasksApplication.class, args);
	}

}
