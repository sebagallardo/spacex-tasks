# SpaceX Team Tasks (NaNLabs exercise)

Trello tasks manager for the SpaceX management team.

This application uses an open-source Java Wrapper for the Trello REST API. (https://github.com/taskadapter/trello-java-wrapper)

## Application setup

### Requirements ###

To run this application you will need **Java 11** and a Trello board with 2 lists: `To Do` and `Selected`.

### Configuration ###
Get your Trello API key and access token https://trello.com/app-key.

Use the template `secrets.properties.example` to create the configuration file `secrets.properties` which contains the Trello API key and access token, and the team's board name. 

### How to run ###
#### CLI
```
mvn install && mvn clean spring-boot:run
```
#### IDE
Import dependencies, build the proyect and execute `SpaceXTaskApplication.java` file.

## Examples
In the root of the project there is a file with a Postman collection to import and run some examples. 

## Improvements ##

Some improvements for a future iteration.

- Improve the assignation of Labels to a Trello card.
- Exception handlers.
- Enhance the endpoint response information.
- Unit and integration tests.
 